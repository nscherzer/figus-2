//
//  FigusAppDelegate.h
//  Figus
//
//  Created by Nicolás Scherzer on 24/12/13.
//  Copyright (c) 2013 Figus. All rights reserved.
//

#import <Parse/Parse.h>
#import <UIKit/UIKit.h>

@interface FigusAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
