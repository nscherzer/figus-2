//
//  main.m
//  Figus
//
//  Created by Nicolás Scherzer on 24/12/13.
//  Copyright (c) 2013 Figus. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FigusAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FigusAppDelegate class]));
    }
}
